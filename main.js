
let chess = [[], [], [], [], [], [], [], []];
let chessboard = document.getElementById('chessboard');

const buildUI = (chess) => {
    let element = document.createElement("div");
    element.setAttribute("class", "chessline");
    chessboard.appendChild(element);  

    let elementb = document.createElement("div");
    elementb.setAttribute("class", "chessblack");

    let elementw = document.createElement("div");
    elementw.setAttribute("class", "chesswhite");

    for(let i = 0; i < chess.length; i++) {
        if (chess[i] % 2 === 0) {
            element.appendChild(elementw.cloneNode(true));
        } else {
            element.appendChild(elementb.cloneNode(true));
        }
    }

}

const lineSorting = (bool) => {
    if (bool) {
        for (let i = 0; i < chess.length; i++) {
            if (i % 2 === 0) {
                chess[i] = 0;
            } else {
                chess[i] = 1;
            }
        }
    } else {
        for (let i = 0; i < chess.length; i++) {
            if (i % 2 === 0) {
                chess[i] = 1;
            } else {
                chess[i] = 0;
            }
        }
    }
    buildUI(chess);
}

let line = 0;
let start = true;

for(let i = 0; i < chess.length; i++) {
    while(line < 8) {
        lineSorting(start);
        start = !start;
        line++;
    }
}
